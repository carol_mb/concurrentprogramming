package concurrentprogramming;

public class Main {
	/**
	 * Cada Stage representa uma etapa, executa-se os testes solicitados na especificação.
	 * O resultados das estatísticas fica salvo em um .txt.
	 * @param args
	 */
	public static void main(String[] args) {
		new Stage1().start();
		new Stage2().start();
		new Stage3().start();
		new Stage4().start();
	}
	
}
