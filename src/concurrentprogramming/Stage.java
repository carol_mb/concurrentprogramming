package concurrentprogramming;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Parte comum a todos os Stages.  
 * C�lculo das estat�sticas e salvar em arquivo.
 */
public class Stage {
	
	/**
	 * Calcula os valores para as estat�sticas (m�ximo, m�nimo, m�dia e desvio padr�o) em segundos.
	 * @param values valor m�ximo, valor m�nimo, valor m�dio
	 * @return estrutura com os dados calculados
	 */
	protected Stat getStat(long[] values) {
		Stat s = new Stat();
		s.max = values[0];
		s.min = values[0];
		s.mean = 0;
		for(int i = 0; i < values.length; i++) {
			if (values[i] < s.min)
				s.min = values[i];
			if (values[i] > s.max)
				s.max = values[i];
			s.mean += values[i];
		}
		s.mean /= values.length;
		for(int i = 0; i < values.length; i++) {
			s.sd += (values[i] - s.mean) * (values[i] - s.mean);
		}
		s.sd = (long) Math.sqrt(s.sd);
		return s;
	}
	
	/**
	 * Salva os valores em um arquivo para a etapa correspondente.
	 * @param name etapa
	 * @param sample n�meros de threads
	 * @param s estat�sticas geradas em cada teste
	 */
	protected void saveFile(String name, int[] sample, Stat[] s) {
		try {
			FileWriter writer = new FileWriter(name);
			for(int i = 0; i < sample.length; i++) {
				writer.write(("" + sample[i]) 
						+ '\t' + '\t' + s[i].mean 
						+ '\t' + '\t' + s[i].min 
						+ '\t' + '\t' + s[i].max 
						+ '\t' + '\t' + s[i].sd);
				writer.write('\n');
			}
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
