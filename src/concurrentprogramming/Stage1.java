package concurrentprogramming;

import java.util.Date;

/**
 * Etapa 1.
 */
public class Stage1 extends Stage {

	private Request[] requests;
	private Thread[] consumers;
	private int last;
	
	public Stage1() {
	}
	
	/**
	 * Reinicializa todas as vari�veis necess�rias para os testes.
	 * Cria o os pedidos. Coloca os pedidos como pendentes no buffer.
	 */
	public void reset() {
		requests = new Request[5000];
		for(int i = 0; i < 5000; i++) {
			requests[i] = new Request();
			requests[i].data = "PENDENTE";
			requests[i].id = i;
		}
		last = 4999;
	}
	
	/**
	 * Inicializa os testes, executando 10 vezes para cada quantidade de threads determinada no int[] sizes.
	 */
	public void start() {
		int[] sizes = new int[] {1, 5, 10, 50, 100, 500, 1000};
		Stat[] stat = new Stat[sizes.length];
		for(int i = 0; i < sizes.length; i++) {
			long[] result = new long[10];
			for(int j = 0; j < 10; j++) {
				reset();
				long before = new Date().getTime();
				test(sizes[i]);
				long after = new Date().getTime();
				result[j] = after - before;
			}
			stat[i] = getStat(result);
		}
		saveFile("stage1.txt", sizes, stat);
	}
	
	/**
	 * Inicializa as threads que representam os consumidos.
	 * @param n n�mero de threads para os consumidores
	 */
	public void test(int n) {
		System.out.println("Start test: " + n);
		consumers = new Thread[n];
		for(int i = 0; i < n; i++) {
			consumers[i] = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						while(last >= 0) {
							Request req = requests[last];
							String date = " In�cio: " + new Date().toGMTString();
							last --;
							req.data = "PROCESSANDO";
							Thread.sleep(30);
							date += " Final: " + new Date().toGMTString();
							System.out.println(Thread.currentThread().getName() + " processou o pedido " + req.id + date);
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}, "Agente " + i);
			consumers[i].start();
		}
		for(int i = 0; i < n; i++) {
			try {
				consumers[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("End test: " + n);
	}

}
