package concurrentprogramming;

import java.util.Date;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Etapa 3.
 */
public class Stage3 extends Stage {
	
	public static int bufferSize = 1000;
	public static int maxSize = 5000;
	
	final Lock startProduceLock = new ReentrantLock();
	final Lock startConsumeLock = new ReentrantLock();
	final Lock finishProduceLock = new ReentrantLock();
	final Lock finishConsumeLock = new ReentrantLock();
	
	final Lock fullLock = new ReentrantLock();
	final Lock emptyLock = new ReentrantLock();
	
	final Condition notFull = fullLock.newCondition();
	final Condition notEmpty = emptyLock.newCondition();

	private Request[] requests;
	private Thread[] consumers;
	private Thread[] producers;
	private int totalProduced;
	private int totalConsumed;
	
	/**
	 * Reinicializa todas as vari�veis necess�rias para os testes.
	 */
	void reset() {
		requests = new Request[bufferSize];
		totalProduced = 0; 
		totalConsumed = 0;
	}
	
	/**
	 * Inicializa os testes, executando 10 vezes para cada quantidade de threads determinada no int[] sizes.
	 */
	public void start() {
		int[] sizes = new int[] {1, 5, 10, 50, 100, 500, 1000};
		Stat[] stat = new Stat[sizes.length];
		for(int i = 0; i < sizes.length; i++) {
			System.out.println("Start test: " + i);
			long[] result = new long[10];
			for(int j = 0; j < 10; j++) {
				reset();
				long before = new Date().getTime();
				test(sizes[i]);
				long after = new Date().getTime();
				result[j] = after - before;
			}
			System.out.println("End test: " + i);
			stat[i] = getStat(result);
		}
		saveFile("stage3.txt", sizes, stat);
	}
	
	/**
	 * Busca um espa�o no buffer para colocar um novo pedido.
	 * @return id do espa�o encontrado
	 */
	private synchronized int findEmpty() {
		for(int i = 0; i < bufferSize; i++) {
			if (requests[i] == null) {
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * Retorna true caso tenha sido produzido um novo pedido, falso caso contr�rio.
	 * @return true caso tenha sido produzido um novo pedido com sucesso
	 */
	private boolean produce() {
		Request req = startProduce();
		if (req == null)
			return false;
		try {
			Thread.sleep(30);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		finishProduce(req);
		return true;
	}
	
	/**
	 * Verifica a quantidade de pedidos produzidos, caso ainda n�o tenha chegado ao m�ximo tenta produzir um novo.
	 * @return pedido produzido
	 */
	private Request startProduce() {
		startProduceLock.lock();
		if (totalProduced >= maxSize) {
			startProduceLock.unlock();
			return null;
		}
		System.out.println(Thread.currentThread().getName() + " est� tentando produzir" + " In�cio: " + new Date().toGMTString());
		int i = findEmpty();
		while(i == -1) {
			fullLock.lock();
			try {
				notFull.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			fullLock.unlock();
			if (totalProduced >= maxSize) {
				startProduceLock.unlock();
				return null;
			}
			i = findEmpty();
		}
		Request req = new Request();
		requests[i] = req;
		req.id = totalProduced;
		req.data = "PRODUZINDO";
		totalProduced++;
		startProduceLock.unlock();
		return req;
	}
	
	/**
	 * Finaliza a produ��o de um pedido.
	 * @param req pedido a ser adicionado no buffer
	 */
	private void finishProduce(Request req) {
		finishProduceLock.lock();
		req.data = "PENDENTE";
		System.out.println(Thread.currentThread().getName() + " produziu o pedido " + req.id + " Final: " + new Date().toGMTString());
		emptyLock.lock();
		notEmpty.signal();
		emptyLock.unlock();
		finishProduceLock.unlock();
	}
	
	/**
	 * Procura um pedido pendente para consumir.
	 * @return id do pedido encontrado
	 */
	private synchronized int findPending() {
		for(int i = 0; i < bufferSize; i++) {
			if (requests[i] != null && requests[i].data.equals("PENDENTE")) {
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * Tenta consumir um pedido.
	 * @return true caso haja sucesso em consumir
	 */
	private boolean consume() {
		int id = startConsume();
		if (id == -1)
			return false;
		try {
			Thread.sleep(30);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		finishConsume(id);
		return true;
	}
	
	/**
	 * Verifica se n�o foi consumida a quantidade m�xima de pedidos, caso ainda seja poss�vel ent�o vai consumir um pedido do buffer.
	 * @return id do pedido consumido
	 */
	private int startConsume() {
		startConsumeLock.lock();
		if (totalConsumed >= maxSize) {
			startConsumeLock.unlock();
			return -1;
		}
		System.out.println(Thread.currentThread().getName() + " est� tentando consumir" + " In�cio: " + new Date().toGMTString());
		int i = findPending();
		while(i == -1) {
			emptyLock.lock();
			try {
				notEmpty.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			emptyLock.unlock();
			if (totalConsumed >= maxSize) {
				startConsumeLock.unlock();
				return -1;
			}
			i = findPending();
		}
		Request req = requests[i];
		req.data = "PROCESSANDO";
		totalConsumed++;
		//System.out.println(totalConsumed);
		startConsumeLock.unlock();
		return i;
	}
	
	/**
	 * Finaliza o processo de consumo de um pedido.
	 * @param i id do pedido consumido
	 */
	private void finishConsume(int i) {
		finishConsumeLock.lock();
		Request req = requests[i];
		System.out.println(Thread.currentThread().getName() + " processou o pedido " + req.id + " Final: " + new Date().toGMTString());
		requests[i] = null;
		fullLock.lock();
		notFull.signal();
		fullLock.unlock();
		finishConsumeLock.unlock();
	}
	
	/**
	 * Inicializa as threads que representam os consumidos e os produtores.
	 * @param n n�mero de threads para os consumidores e os produtores
	 */
	public void test(int n) {
		producers = new Thread[n];
		for(int i = 0; i < n; i++) {
			producers[i] = new Thread(new Runnable() {
				@Override
				public void run() {
					while(totalProduced < maxSize) {
						if (!produce())
							break;
					}
				}
			}, "Produtor " + i);
			producers[i].start();
		}
		consumers = new Thread[n];
		for(int i = 0; i < n; i++) {
			consumers[i] = new Thread(new Runnable() {
				@Override
				public void run() {
					while(totalConsumed < maxSize) {
						if (!consume())
							break;
					}
				}
			}, "Consumidor " + i);
			consumers[i].start();
		}
		
		try {
			for(int i = 0; i < n; i++) {
				producers[i].join();
			}
			for(int i = 0; i < n; i++) {
				consumers[i].join();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
