package concurrentprogramming;

/**
 * Estrutura para representar valor m�ximo, m�nimo, m�dia e desvio padr�o.
 */
public class Stat {
	
	public long min;
	public long max;
	public double mean;
	public double sd;
	
}
